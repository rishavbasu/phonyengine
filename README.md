# PhonyEngine

## Project Details
PhonyEngine is a audio engine for me to test out different audio techniques. Currently I've been testing simple additive synthesizers and 3D audio localization. I intend to continue developing features for PhonyEngine including audio occulsion, HRTF, reverb, audio filtering, and more.
