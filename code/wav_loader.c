
#include "wav_loader.h"
#include "audio.h"
#include <stdio.h>
#include <stdlib.h>

PHONY_API b32 read_wav_file(const char* file_name, sound_data* data)
{
    FILE* file = fopen(file_name, "rb");
    if(!file)
        return false;
    
    fseek(file, 0, SEEK_END);
    int length = ftell(file);
    char* contents = (char*) malloc(length);
    fseek(file, 0, SEEK_SET);
    fread(contents, length, 1, file);
    
    wav_file_header* header = (wav_file_header*) contents;
    if(header->chunk_id      != RIIF_ID_RIFF ||
       header->format_name   != RIIF_ID_WAVE ||
       header->fmt_chunk_id  != RIIF_ID_FMT  ||
       header->data_chunk_id != RIIF_ID_DATA)
    {
        printf("WAV file header doesn't match spec!\n");
        return false;
    }
    
    data->sample_rate     = header->sample_rate;
    data->num_samples     = header->data_chunk_size / ( header->bits_per_sample / 8 );
    data->bits_per_sample = header->bits_per_sample;
    data->bytes_per_sample = data->bits_per_sample / 8;
    data->num_channels    = header->num_channels;
    data->data_size       = header->data_chunk_size;
    data->data            = (u8*) malloc(data->data_size);
    
    memcpy(data->data, &header->start_of_data, data->data_size);
    
    free(contents);
    
    return true;
}