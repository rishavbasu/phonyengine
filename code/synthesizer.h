/* date = March 9th 2022 8:52 pm */

#ifndef SYNTHESIZER_H
#define SYNTHESIZER_H

#include "common_types.h"

typedef enum
{
    SYNTH_WAVE_TYPE_SINE,
    SYNTH_WAVE_TYPE_TRIANGLE,
    SYNTH_WAVE_TYPE_SQUARE,
    SYNTH_WAVE_TYPE_SAWTOOTH,
    SYNTH_WAVE_TYPE_SEMISINE,
    SYNTH_WAVE_TYPE_COUNT
} synth_wave_type;

typedef struct synth
{
    synth_wave_type WaveType;
    r32 Frequency;
    r32 Time;
    u32 NumHarmonics;
} synth;

PHONY_API void WriteSynthToBuffer(r32* Buffer, u32 BufferOffset, u32 BufferSize, synth* Synth, u32 NumFrames, u32 NumChannels, r32 SampleRate, r32 Pan);


#endif //SYNTHESIZER_H
