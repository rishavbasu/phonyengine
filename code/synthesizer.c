
#include "synthesizer.h"
#include <math.h>
#include <stdio.h>

r32 TriangleWave(r32 X)
{
    // Could maybe be faster by dividing by tau first and then using some FP magic
    r32 ZeroToTau = (r32) fmod(X, 6.283f);
    return ZeroToTau / 6.283f - 0.5f;
}

r32 SquareWave(r32 X)
{
    r32 ZeroToTau = (r32) fmod(X, 6.283f);
    return ZeroToTau < 3.1415f ? -1.0f : 1.0f;
}

r32 SawtoothWave(r32 X)
{
    r32 ZeroToTau = (r32) fmod(X, 6.283f);
    r32 MinusPiToPi = ZeroToTau - 3.1415f;
    if(MinusPiToPi < 0.0f)
    {
        return 2.0f * (MinusPiToPi / 3.1415f) + 1.0f;
    }
    else 
    {
        return 2.0f * (-MinusPiToPi / 3.1415f) + 1.0f;
    }
}

r32 SemisineWave(r32 X)
{
    r32 AbsSine = (r32) fabs(sin(0.5f * X));
    return 2.0f * AbsSine - 1.0f;
}

PHONY_API void WriteSynthToBuffer(r32* Buffer, u32 BufferOffset, u32 BufferSize, synth* Synth, u32 NumFrames, u32 NumChannels, r32 SampleRate, r32 Pan)
{
    u32 BufferPos = BufferOffset;
    const r32 SampleOffset = Synth->Time;
    r32 LeftPan = 1.0f - Pan;
    r32 RightPan = Pan;
    r32 HarmonicRatios[] = { 1.0f, 0.5f, 0.25f, 0.125f, 0.0625f, 0.03125f, 0.015625f };
    // Faster to switch before entering the loop but not as pretty :(
    switch(Synth->WaveType)
    {
        case SYNTH_WAVE_TYPE_SINE:
        {
            for(u32 Idx = 0; Idx < NumFrames; ++Idx)
            {
                r32 Time = SampleOffset + (Idx / SampleRate);
                r32 Sample = 0.0f;
                for(u32 Harmonic = 0; Harmonic < Synth->NumHarmonics; ++Harmonic)
                {
                    Sample += (r32) sin(6.283f * Time * Synth->Frequency * (Harmonic + 1)) * 0.25f * HarmonicRatios[Harmonic];
                }
                Buffer[BufferPos++ % BufferSize] += LeftPan * Sample;
                Buffer[BufferPos++ % BufferSize] += RightPan * Sample;
            }
        } break;
        case SYNTH_WAVE_TYPE_TRIANGLE:
        {
            for(u32 Idx = 0; Idx < NumFrames; ++Idx)
            {
                r32 Time = SampleOffset + (Idx / SampleRate);
                r32 Sample = 0.0f;
                for(u32 Harmonic = 0; Harmonic < Synth->NumHarmonics; ++Harmonic)
                {
                    Sample += TriangleWave(6.283f * Time * Synth->Frequency * (Harmonic + 1)) * 0.25f * HarmonicRatios[Harmonic];
                }
                Buffer[BufferPos++ % BufferSize] += LeftPan * Sample;
                Buffer[BufferPos++ % BufferSize] += RightPan * Sample;
            }
        } break;
        case SYNTH_WAVE_TYPE_SQUARE:
        {
            for(u32 Idx = 0; Idx < NumFrames; ++Idx)
            {
                r32 Time = SampleOffset + (Idx / SampleRate);
                r32 Sample = 0.0f;
                for(u32 Harmonic = 0; Harmonic < Synth->NumHarmonics; ++Harmonic)
                {
                    Sample += SquareWave(6.283f * Time * Synth->Frequency * (Harmonic + 1)) * 0.25f * HarmonicRatios[Harmonic];
                }
                Buffer[BufferPos++ % BufferSize] += LeftPan * Sample;
                Buffer[BufferPos++ % BufferSize] += RightPan * Sample;
            }
        } break;
        case SYNTH_WAVE_TYPE_SAWTOOTH:
        {
            for(u32 Idx = 0; Idx < NumFrames; ++Idx)
            {
                r32 Time = SampleOffset + (Idx / SampleRate);
                r32 Sample = 0.0f;
                for(u32 Harmonic = 0; Harmonic < Synth->NumHarmonics; ++Harmonic)
                {
                    Sample += SawtoothWave(6.283f * Time * Synth->Frequency * (Harmonic + 1)) * 0.25f * HarmonicRatios[Harmonic];
                }
                Buffer[BufferPos++ % BufferSize] += LeftPan * Sample;
                Buffer[BufferPos++ % BufferSize] += RightPan * Sample;
            }
        } break;
        case SYNTH_WAVE_TYPE_SEMISINE:
        {
            for(u32 Idx = 0; Idx < NumFrames; ++Idx)
            {
                r32 Time = SampleOffset + (Idx / SampleRate);
                r32 Sample = 0.0f;
                for(u32 Harmonic = 0; Harmonic < Synth->NumHarmonics; ++Harmonic)
                {
                    Sample += SemisineWave(6.283f * Time * Synth->Frequency * (Harmonic + 1)) * 0.25f * HarmonicRatios[Harmonic];
                }
                Buffer[BufferPos++ % BufferSize] += LeftPan * Sample;
                Buffer[BufferPos++ % BufferSize] += RightPan * Sample;
            }
        } break;
        default:
        {
        } break;
    }
    
    Synth->Time += ((r32) NumFrames) / (SampleRate);
}