
#define IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#include "vendors\imgui\imgui.cpp"
#include "vendors\imgui\backends\imgui_impl_opengl3.cpp"
#include "vendors\imgui\backends\imgui_impl_glfw.cpp"
#include "vendors\imgui\imgui_draw.cpp"
#include "vendors\imgui\imgui_widgets.cpp"
#include "vendors\imgui\imgui_tables.cpp"
#include "vendors\imgui\imgui_plot.cpp"