
#ifndef QUARTZ_PLATFORM_BACKEND_H
#define QUARTZ_PLATFORM_BACKEND_H

#include "common_types.h"

typedef struct window_handle
{
	u64 Opaque;
} window_handle;

typedef struct platform_api
{
    b32 (*InitializeAudio)();
    u32 (*FramesToWrite)();
    void (*WriteSoundBuffer)(r32* Samples, u32 NumFrames);
} platform_api;

#endif QUARTZ_PLATFORM_BACKEND_H