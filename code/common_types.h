
#ifndef PHONY_COMMON_TYPES_H
#define PHONY_COMMON_TYPES_H

#include <stdint.h>
#include <stdbool.h>
#ifdef PHONY_DEBUG
#define PHONY_ENABLE_ASSERT 1
#endif

#define Stmnt(S) do{ S }while(0)
#define AssertBreak() (*(int*)0 = 0)

#if PHONY_ENABLE_ASSERT
#define Assert(C) Stmnt( if(!C) { AssertBreak(); } ) 
#else
#define Assert(C)
#endif

#define ArrayCount(arr) (sizeof(arr)/(sizeof(arr[0])))
#define CONCATONATE_DIRECT(s1, s2) s1##s2
#define CONCATONATE(s1, s2) CONCATONATE_DIRECT(s1, s2)

#define PY_MIN(a, b) (a) < (b) ? (a) : (b)
#define PY_MAX(a, b) (a) > (b) ? (a) : (b)

#define KILOBYTES(N) (1024 * N)
#define MEGABYTES(N) (1024 * KILOBYTES(N))
#define GIGABYTES(N) (1024 * MEGABYTES(N))


typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef i32 b32;
typedef i8 b8;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef i8 c8;

typedef float r32;
typedef double r64;

static u32 U32_MAX = ~((u32) 0);
static u64 U64_MAX = (u64) ~0;

#ifdef __cplusplus
#define PHONY_API extern "C"
#else
#define PHONY_API
#endif

typedef union vec2
{
    struct
    {
        r32 x, y;
    };
    
    r32 e[2];
} vec2;

#endif // PHONY_COMMON_TYPES_H