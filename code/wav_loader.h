/* date = March 19th 2022 2:49 pm */

#ifndef WAV_LOADER_H
#define WAV_LOADER_H

#include "common_types.h"

#define ASCII_ID_4(a, b, c, d) ((a) | ((b) << 8) | ((c) << 16) | ((d) << 24))

enum riif_id
{
    RIIF_ID_RIFF = ASCII_ID_4('R', 'I', 'F', 'F'),
    RIIF_ID_WAVE = ASCII_ID_4('W', 'A', 'V', 'E'),
    RIIF_ID_FMT  = ASCII_ID_4('f', 'm', 't', ' '),
    RIIF_ID_DATA = ASCII_ID_4('d', 'a', 't', 'a'),
};

typedef struct wav_file_header
{
    // RIFF Header
    u32 chunk_id;
    u32 chunk_size;
    u32 format_name;
    
    // fmt chunk
    u32 fmt_chunk_id;
    u32 fmt_chunk_size;
    u16 audio_format; // 1 = PCM, other values indicate some compression
    u16 num_channels;
    u32 sample_rate;
    u32 byte_rate;  // Bytes for 1 frame
    u16 block_align;
    u16 bits_per_sample;
    // TODO: If reading compress .wav files, there might be extra values here
    
    // data chunk
    u32 data_chunk_id;
    u32 data_chunk_size; 
    
    u8 start_of_data;
} wav_file_header;

typedef struct sound_data sound_data;

PHONY_API b32 read_wav_file(const char* file_name, sound_data* data);

#endif //WAV_LOADER_H
