#include <stdio.h>
#include <windows.h>
#include <mmdeviceapi.h>
#include <math.h>
#include "win32_wasapi_backend.h"
#include "audio.h"
#include "synthesizer.h"
#include "wav_loader.h"
#include "vendors/glad/include/glad/glad.h"
#include "vendors/glfw/include/GLFW/glfw3.h"
#include "imgui_backend.cpp"
//#include "imgui_extensions.cpp"
#include "vendors/DearWidgets/src/api/dear_widgets.cpp"

b32 Running = true;

struct synth_payload {
    b32 InputKeys[13];
    synth Synths[13];
    r32 Frequencies[13];
    r32 BaseFrequency;
    synth_wave_type CurrWaveType;
    i32 NumHarmonics;
    r32 Pan;
    GLFWwindow* Window;
};
synth_payload SynthPayload;

audio_system AudioSystem;
ImVec2 pos;

void Win32ProcessPendingMessages()
{
    MSG Message;
    while(PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
    {
        switch(Message.message)
        {
            case WM_QUIT:
            {
                Running = false;
            } break;
            case WM_SYSKEYDOWN:
            case WM_SYSKEYUP:
            case WM_KEYDOWN:
            case WM_KEYUP:
            default:
            {
                TranslateMessage(&Message);
                DispatchMessage(&Message);
            } break;
        }
    }
}

void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if(key == GLFW_KEY_SPACE && action == GLFW_PRESS) {
        u32 voice = play_sound_at_pos_2d(&AudioSystem, 0, { pos.x, pos.y });
        //fade_voice_out(&AudioSystem, voice, 2.0f);
    }
}

void GLFWErrorCallback(i32 error, const char* description)
{
    printf("Error: %s\n", description);
}

int CALLBACK
WinMain(HINSTANCE Instance,
        HINSTANCE PrevInstance,
        LPSTR     CommandLine,
        int       ShowCommand
        )
{
    platform_api Win32PlatformAPI = GetWin32WASAPIBackend();
    if(Win32PlatformAPI.InitializeAudio())
    {
        printf("Successful Initialization!\n");
    }
    else
    {
        printf("Bad Init!\n");
    }
    
    if(!glfwInit())
    {
        printf("Error initializing glfw!\n");
    }
    else
    {
        printf("Initialized glfw!\n");
    }
    glfwSetErrorCallback(GLFWErrorCallback);
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    GLFWwindow* Window = glfwCreateWindow(1920, 1080, "Quartz Board", NULL, NULL);
    if (!Window)
    {
        printf("Failed to create window!\n");
    }
    glfwMakeContextCurrent(Window);
    gladLoadGL();
    glfwSetKeyCallback(Window, KeyCallback);
    glfwSwapInterval(1);
    
    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    //ImPlot::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.Fonts->AddFontFromFileTTF("SpaceGrotesk-VariableFont_wght.ttf", 24.0f);
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
    //io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows
    
    ImGui::StyleColorsDark();
    
    // When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
    ImGuiStyle& style = ImGui::GetStyle();
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        style.WindowRounding = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 1.0f;
    }
    
    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(Window, true);
    ImGui_ImplOpenGL3_Init("#version 150");
    
    bool show_demo_window = true;
    bool show_another_window = false;
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
    
    r32 Keys[13][96000] = { 0 };
    r32 Frequencies[13] = { 1.0f,
        9.0f / 8.0f,
        5.0f / 4.0f,
        4.0f / 3.0f,
        3.0f / 2.0f,
        13.0f / 8.0f,
        15.0f / 8.0f,
        256.0f / 243.0f,
        6.0f / 5.0f,
        729.0f / 512.0f,
        128.0f / 81.0f,
        16.0f / 9.0f,
        2.0f };
    SynthPayload.BaseFrequency = 200.0f; // 200 HZ
    
    for(u32 Key = 0; Key < 13; ++Key)
    {
        SynthPayload.Frequencies[Key] = Frequencies[Key];
        SynthPayload.Synths[Key].Frequency = SynthPayload.BaseFrequency * Frequencies[Key];
    }
    
    u32 LastAudioFrameCount = 0;
    u32 LastStartIndex = 0;
    SynthPayload.CurrWaveType = SYNTH_WAVE_TYPE_SINE;
    SynthPayload.NumHarmonics = 1;
    SynthPayload.Pan = 0.5f;
    SynthPayload.Window = Window;
    
    load_sound_from_filename(&AudioSystem, "assets/sfx/Pop.wav");
    
    auto SynthFunc = [](audio_bus* bus, u32 num_frames, u32 num_channels, void* payload) {
        u32 NumSamples = num_frames * num_channels;
        u32 StartIndex = (bus->frames_played % 48000) * 2;
        synth_payload* Synth = (synth_payload*) payload;
        i32 KeysForNotes[] = { GLFW_KEY_A, GLFW_KEY_S, GLFW_KEY_D, GLFW_KEY_F, GLFW_KEY_G, GLFW_KEY_H, GLFW_KEY_J, GLFW_KEY_W, GLFW_KEY_E, GLFW_KEY_T, GLFW_KEY_Y, GLFW_KEY_U, GLFW_KEY_K  };
        for(u32 Idx = 0; Idx < 13; ++Idx) {
            Synth->InputKeys[Idx] = glfwGetKey(Synth->Window, KeysForNotes[Idx]);
        }
        
        b32 UpOctave = glfwGetKey(Synth->Window, GLFW_KEY_RIGHT_SHIFT);
        b32 DownOctave = glfwGetKey(Synth->Window, GLFW_KEY_LEFT_SHIFT);
        u32 TotalNotes = 0;
        for(u32 Key = 0; Key < 13; ++Key) {
            if(Synth->InputKeys[Key]) {
                TotalNotes++;
                if(UpOctave)
                    Synth->Synths[Key].Frequency = SynthPayload.BaseFrequency * SynthPayload.Frequencies[Key] * 2.0f;
                else if(DownOctave)
                    Synth->Synths[Key].Frequency = SynthPayload.BaseFrequency * SynthPayload.Frequencies[Key] * 0.5f;
                else
                    Synth->Synths[Key].Frequency = SynthPayload.BaseFrequency * SynthPayload.Frequencies[Key] * 1.0f;
                Synth->Synths[Key].WaveType = Synth->CurrWaveType;
                Synth->Synths[Key].NumHarmonics = Synth->NumHarmonics;
                WriteSynthToBuffer(&bus->buffer[0], (bus->frames_played % 48000) * 2, 96000, &Synth->Synths[Key], num_frames, 2, 48000.0f, Synth->Pan);
            }
        }
        
        if(TotalNotes) {
            for(u32 Sample = 0; Sample < NumSamples; ++Sample) {
                bus->buffer[(StartIndex + Sample) % 96000] /= TotalNotes;
            }
        }
    };
    add_sythesizer_voice(&AudioSystem, SynthFunc, &SynthPayload);
    
    while (!glfwWindowShouldClose(Window))
    {
        glfwPollEvents();
        
        if(glfwGetKey(Window, GLFW_KEY_ESCAPE))
        {
            glfwSetWindowShouldClose(Window, GLFW_TRUE);
            continue;
        }
        
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        
        ImGui::Begin("Quartz Board");
        
        ImGui::SliderFloat("Panning", &SynthPayload.Pan, 0.0f, 1.0f);
        
        i32 WaveType = SynthPayload.CurrWaveType;
        ImGui::Text("Wave Shape");
        ImGui::RadioButton("Sine", &WaveType, 0); ImGui::SameLine();
        ImGui::RadioButton("Triangle", &WaveType, 1); ImGui::SameLine();
        ImGui::RadioButton("Square", &WaveType, 2); ImGui::SameLine();
        ImGui::RadioButton("Sawtooth", &WaveType, 3); ImGui::SameLine();
        ImGui::RadioButton("Semisine", &WaveType, 4);
        SynthPayload.CurrWaveType = (synth_wave_type) WaveType;
        
        ImGui::SliderInt("Number of Harmonics", &SynthPayload.NumHarmonics, 1, 7);
        
        u32 AudioFrameCount = Win32PlatformAPI.FramesToWrite();
        u32 NumSamples = AudioFrameCount * 2; // assumes 2 channels
        u32 StartIndex = (AudioSystem.master_bus.frames_played % 48000) * 2;
        if(AudioFrameCount > 0)
        {
            render_audio(&AudioSystem, AudioFrameCount);
        }
        Win32PlatformAPI.WriteSoundBuffer(AudioSystem.master_bus.buffer, AudioFrameCount);
        AudioSystem.master_bus.frames_played += AudioFrameCount;
        
        ImGui::Text("Frames: %d", LastAudioFrameCount);
        {
            r32 SamplesForChannel1[48000] = {};
            for(u32 Sample = 0; Sample < LastAudioFrameCount; ++Sample)
            {
                SamplesForChannel1[Sample] = 2.0f * AudioSystem.master_bus.buffer[(LastStartIndex + 2 * Sample) % 96000];
            }
            ImGui::PlotConfig Cfg = {};
            Cfg.values.ys = SamplesForChannel1;
            Cfg.values.count = LastAudioFrameCount;
            Cfg.values.offset = 0;
            Cfg.scale.min = -1.0f;
            Cfg.scale.max = 1.0f;
            Cfg.frame_size = ImVec2(1080.0f, 360.0f);
            ImGui::Plot("AudioBuffer", Cfg);
        }
        {
            r32 SamplesForChannel2[48000] = {};
            for(u32 Sample = 0; Sample < LastAudioFrameCount; ++Sample)
            {
                SamplesForChannel2[Sample] = 2.0f * AudioSystem.master_bus.buffer[(LastStartIndex + 2 * Sample + 1) % 96000];
            }
            ImGui::PlotConfig Cfg = {};
            Cfg.values.ys = SamplesForChannel2;
            Cfg.values.count = LastAudioFrameCount;
            Cfg.values.offset = 0;
            Cfg.scale.min = -1.0f;
            Cfg.scale.max = 1.0f;
            Cfg.frame_size = ImVec2(1080.0f, 360.0f);
            ImGui::Plot("AudioBuffer", Cfg);
        }
        
        ImWidgets::InputVec2("Test", &pos, ImVec2(-10.0f, -10.0f), ImVec2(10.0f, 10.0f), 1.0f);
        ImGui::End();
        
        // Rendering
        ImGui::Render();
        i32 display_w, display_h;
        glfwGetFramebufferSize(Window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        
        // Update and Render additional Platform Windows
        // (Platform functions may change the current OpenGL context, so we save/restore it to make it easier to paste this code elsewhere.
        //  For this specific demo app we could also call glfwMakeContextCurrent(window) directly)
        if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
        {
            GLFWwindow* backup_current_context = glfwGetCurrentContext();
            ImGui::UpdatePlatformWindows();
            ImGui::RenderPlatformWindowsDefault();
            glfwMakeContextCurrent(backup_current_context);
        }
        
        glfwSwapBuffers(Window);
        
        LastAudioFrameCount = AudioFrameCount > 0 ? AudioFrameCount : LastAudioFrameCount;
        LastStartIndex = AudioFrameCount > 0 ? StartIndex : LastStartIndex ;
        
        fflush(stdout);
    }
    
    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    //ImPlot::DestroyContext();
    ImGui::DestroyContext();
    
    glfwDestroyWindow(Window);
    glfwTerminate();
    
    return 0;
}
