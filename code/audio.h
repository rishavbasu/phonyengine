/* date = March 7th 2022 0:48 am */

#ifndef AUDIO_H
#define AUDIO_H

#include "common_types.h"

#define MAX_SOUNDS_LOADED 32u
#define VOICE_INDEX_BITS 8u
#define VOICE_GENERATION_BITS (32u - VOICE_INDEX_BITS)
#define VOICE_INDEX_MASK ((~0u) >> VOICE_GENERATION_BITS)
#define MAX_VOICES_PLAYING (1 << VOICE_INDEX_BITS)

typedef struct sound_data {
    u32 sample_rate;
    u32 num_samples;
    u32 bits_per_sample;
    u32 bytes_per_sample;
    u32 num_channels;
    u32 data_size;
    u8* data;
} sound_data;

typedef struct audio_bus {
    r32 buffer[96000];
    u64 frames_played;
} audio_bus;

typedef enum voice_state {
    PY_VOICE_STATE_REMOVED = 0,
    PY_VOICE_STATE_PLAYING = 1,
    PY_VOICE_STATE_FADING = 2,
    //PY_VOICE_STATE_STOPPED = 3,
    //PY_VOICE_STATE_STOPPING = 4,
} voice_state;

// Removing the voice_state enum to be more combinatorically efficient
typedef enum voice_flags {
    PY_VOICE_FLAGS_PLAYING           = 1 << 0,
    PY_VOICE_FLAGS_LOADED_SOUND      = 1 << 1,
    PY_VOICE_FLAGS_STREAMED_SOUND    = 1 << 2,
    PY_VOICE_FLAGS_SYNTHESIZED_SOUND = 1 << 3,
    PY_VOICE_FLAGS_2D                = 1 << 5,
    PY_VOICE_FLAGS_3D                = 1 << 6,
    PY_VOICE_FLAGS_FADING            = 1 << 7,
    PY_VOICE_FLAGS_REMOVE_AFTER_FADE = 1 << 8,
} voice_flags;

typedef void (*synthesize_func)(audio_bus*, u32, u32, void*);

typedef struct voice_instance voice_instance;
typedef struct voice_instance {
    voice_flags flags;
    u32 generation;
    i32 current_sample;
    vec2 pos_2d;
    union {
        struct {
            u32 sound_id;
            r32 volume;
            
            // Used for fading
            r32 target_volume;
            i32 fade_start_sample;
            i32 fade_end_sample;
        };
        struct {
            synthesize_func synthesizer;
            void* payload;
        };
        struct {
            voice_instance* next;
            voice_instance* prev;
        };
    };
} voice_instance;

typedef struct audio_system {
    sound_data loaded_sounds[MAX_SOUNDS_LOADED];
    u32 num_loaded_sounds;
    voice_instance voices[MAX_VOICES_PLAYING];
    voice_instance* free_voice_list;
    u32 num_voice_instance;
    
    audio_bus master_bus;
} audio_system;

PHONY_API b32 load_sound_from_filename(audio_system* audio, const char* file_name);
PHONY_API voice_instance* voice_from_id(audio_system* audio, u32 id);
PHONY_API voice_instance* find_free_voice(audio_system* audio);
PHONY_API void remove_voice(audio_system* audio, voice_instance* voice);
PHONY_API void add_voice_flags(voice_instance* voice, voice_flags flags);
PHONY_API void remove_voice_flags(voice_instance* voice, voice_flags flags);
PHONY_API b32 has_voice_flags(voice_instance* voice, voice_flags flags);

PHONY_API void fade_voice_out(audio_system* audio, u32 voice_id, r32 time_to_fade_out);

PHONY_API u32 play_sound(audio_system* audio, u32 sound_id);
PHONY_API u32 play_sound_at_pos_2d(audio_system* audio, u32 sound_id, vec2 pos);
PHONY_API u32 add_sythesizer_voice(audio_system* audio, synthesize_func synthesizer, void* payload);
PHONY_API void render_audio(audio_system* audio, u32 num_frames_to_write);
PHONY_API void reload_audio_system(audio_system* audio);
PHONY_API void free_audio_resources(audio_system* audio);


#endif //AUDIO_H
