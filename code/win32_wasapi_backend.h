#ifndef PHONY_WIN32_WASAPI_BACKEND_H
#define PHONY_WIN32_WASAPI_BACKEND_H

#include "common_types.h"
#include "platform_backend.h"

struct IMMDeviceEnumerator;
struct IMMDevice;
struct IAudioRenderClient;
struct IAudioClient;
struct audio_system;

typedef struct wasapi_audio
{
    struct IMMDeviceEnumerator* DeviceEnum;
    struct IMMDevice* Device;
    struct IAudioRenderClient* AudioRenderClient;
    struct IAudioClient* AudioClient;
    
    i64 SoundBufferDuration;
    u64 FramesPlayed;
    u32 SamplesHz;
    u32 NumChannels;
    u32 LatencyFrameCount;
    u32 BufferFrameCount;
} wasapi_audio;

b32 LoadAndInitializeWASAPI();
u32 FramesToWrite();
void WriteSoundBuffer(r32* Samples, u32 NumFrames);

PHONY_API platform_api GetWin32WASAPIBackend();

#endif // PHONY_WIN32_WASAPI_BACKEND_H
