/* date = February 9th 2023 2:26 am */

#ifndef PHONY_MATH_H
#define PHONY_MATH_H

#include "common_types.h"
#include <math.h>

inline r32 py_r32_min(r32 a, r32 b) {
    return a > b ? b : a;
}

inline r32 py_r32_max(r32 a, r32 b) {
    return a < b ? b : a;
}

inline r32 py_sqrt_r32(r32 x) {
    return (r32) sqrt(x);
}

inline r32 py_log2_r32(r32 x) {
    return log2f(x);
}

inline r32 py_r32_square(r32 x) {
    return x * x;
}

inline r32 py_r32_lerp(r32 a, r32 b, r32 frac) {
    return (b - a) * frac + a;
}

inline vec2 py_vec2_make(r32 x, r32 y) {
    vec2 ret = { x, y };
    return ret;
}

inline vec2 vec2_add(vec2 a, vec2 b) {
    vec2 ret = { a.x + b.x, a.y + b.y };
    return ret;
}

inline vec2 vec2_subtract(vec2 a, vec2 b) {
    vec2 ret = { a.x - b.x, a.y - b.y };
    return ret;
}

inline vec2 vec2_negate(vec2 v) {
    vec2 ret = { -v.x, -v.y };
    return ret;
}

inline vec2 vec2_div_r32(vec2 v, r32 n) {
    vec2 ret = { v.x / n, v.y / n };
    return ret;
}

inline vec2 vec2_mul_r32(r32 const n, vec2 v) {
    vec2 ret = { v.x * n, v.y * n };
    return ret;
}

inline r32 py_vec2_dot(vec2 a, vec2 b) {
    return a.x * b.x + a.y * b.y;
}

inline r32 py_vec2_length_sq(vec2 v) {
    return py_vec2_dot(v, v);
}

inline r32 py_vec2_length(vec2 v) {
    return py_sqrt_r32(py_vec2_length_sq(v));
}

inline r32 py_vec2_dist_sq(vec2 a, vec2 b) {
    return py_vec2_length_sq(vec2_subtract(a, b));
}

inline r32 py_vec2_dist(vec2 a, vec2 b) {
    return py_vec2_length(vec2_subtract(a, b));
}

inline vec2 py_vec2_normalized(vec2 v) {
    return vec2_div_r32(v, py_vec2_length(v));
}

inline vec2 py_vec2_normalized_safe(vec2 v) {
    r32 len = py_vec2_length(v);
    if(len < 0.0001f)
        return v;
    return vec2_div_r32(v, len);
}

inline vec2 py_closest_point_on_segment(vec2 s1, vec2 s2, vec2 p)
{
    vec2 s = vec2_subtract(s2, s1);
    vec2 local_p = vec2_subtract(p, s1);
    r32 t = py_vec2_dot(local_p, s) / py_vec2_length_sq(s);
    if(t < 0.0f)
        return s1;
    else if(t > 1.0f)
        return s2;
    else
        return vec2_add(vec2_mul_r32(t, s), s1);
}

inline r32 py_dist_to_segment_sq(vec2 s1, vec2 s2, vec2 p) {
    vec2 closest_point = py_closest_point_on_segment(s1, s2, p);
    return py_vec2_dist_sq(closest_point, p);
}

#endif //PHONY_MATH_H
