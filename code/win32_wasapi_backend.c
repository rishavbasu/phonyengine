
#include "win32_wasapi_backend.h"
#include <windows.h>
#include <mmdeviceapi.h>
#include "audioclient.h"
#include <stdio.h>
#ifdef _WIN32
#undef MAX
#undef MIN
#endif //_WIN32

extern const GUID IID_IAudioClient = {0x1CB9AD4C, 0xDBFA, 0x4c32, 0xB1, 0x78, 0xC2, 0xF5, 0x68, 0xA7, 0x03, 0xB2};
extern const GUID IID_IAudioRenderClient = {0xF294ACFC, 0x3146, 0x4483, 0xA7, 0xBF, 0xAD, 0xDC, 0xA7, 0xC2, 0x60, 0xE2};
extern const GUID CLSID_MMDeviceEnumerator = {0xBCDE0395, 0xE52F, 0x467C, 0x8E, 0x3D, 0xC4, 0x57, 0x92, 0x91, 0x69, 0x2E};
extern const GUID IID_IMMDeviceEnumerator = {0xA95664D2, 0x9614, 0x4F35, 0xA7, 0x46, 0xDE, 0x8D, 0xB6, 0x36, 0x17, 0xE6};
extern const GUID PcmSubformatGuid = {STATIC_KSDATAFORMAT_SUBTYPE_PCM};

#define SOUND_LATENCY_FPS 10
#define REFTIMES_HZ 10000000

#define CO_CREATE_INSTANCE(name) HRESULT name(REFCLSID rclsid, LPUNKNOWN *pUnkOuter, DWORD dwClsContext, REFIID riid, LPVOID *ppv)
typedef CO_CREATE_INSTANCE(CoCreateInstance_);
static CoCreateInstance_* CoCreateInstanceProc;

#define CO_INITIALIZE_EX(name) HRESULT name(LPVOID pvReserved, DWORD dwCoInit)
typedef CO_INITIALIZE_EX(CoInitializeEx_);
static CoInitializeEx_* CoInitializeExProc;

static wasapi_audio WasapiAudio;

b32 LoadAndInitializeWASAPI()
{
    //PROFILE_SCOPE("Loading and initializing WASAPI");
    
    HMODULE WASAPILib = LoadLibraryA("ole32.dll");
    
    if(WASAPILib == NULL)
    {
        printf("Error loading ole32.dll!");
        return false;
    }
    
    CoCreateInstanceProc = (CoCreateInstance_*) GetProcAddress(WASAPILib, "CoCreateInstance");
    CoInitializeExProc = (CoInitializeEx_*) GetProcAddress(WASAPILib, "CoInitializeEx");
    
    if(CoCreateInstanceProc == NULL || CoInitializeExProc == NULL)
    {
        printf("Error getting WASAPI procedures!");
        return false;
    }
    
    CoInitializeExProc(0, COINIT_SPEED_OVER_MEMORY);
    HRESULT Result = CoCreateInstanceProc(&CLSID_MMDeviceEnumerator,
                                          0,
                                          CLSCTX_ALL,
                                          &IID_IMMDeviceEnumerator,
                                          (LPVOID*) &WasapiAudio.DeviceEnum);
    if(Result != S_OK)
    {
        printf("Error retrieving WASAPI device enumerator!");
        return false;
    }
    
    Result = WasapiAudio.DeviceEnum->lpVtbl->GetDefaultAudioEndpoint(WasapiAudio.DeviceEnum,
                                                                     eRender,
                                                                     eConsole,
                                                                     &WasapiAudio.Device);
    if(Result != S_OK)
    {
        printf("Error getting default audio endpoint for WASAPI!");
        return false;
    }
    
    Result = WasapiAudio.Device->lpVtbl->Activate(WasapiAudio.Device,
                                                  &IID_IAudioClient,
                                                  CLSCTX_ALL,
                                                  0,
                                                  (void**) &WasapiAudio.AudioClient);
    if(Result != S_OK)
    {
        printf("Error activating WASAPI device!");
        return false;
    }
    
    WAVEFORMATEX* WaveFormat = NULL;
    WasapiAudio.AudioClient->lpVtbl->GetMixFormat(WasapiAudio.AudioClient,
                                                  &WaveFormat);
    WasapiAudio.NumChannels = 2;
    WasapiAudio.SamplesHz = 48000;
    WORD BitsPerSample = sizeof(u16) * 8;
    WORD BlockAlign = (WORD) (WasapiAudio.NumChannels * BitsPerSample) / 8;
    DWORD AvgBytesHz = BlockAlign * WasapiAudio.SamplesHz;
    WORD CBSize = 0;
    
    WAVEFORMATEX NewWaveFormat = {0};
    NewWaveFormat.wFormatTag = WAVE_FORMAT_PCM;
    NewWaveFormat.nChannels = (WORD) WasapiAudio.NumChannels;
    NewWaveFormat.nSamplesPerSec = (WORD) WasapiAudio.SamplesHz;
    NewWaveFormat.nAvgBytesPerSec = AvgBytesHz;
    NewWaveFormat.nBlockAlign = BlockAlign;
    NewWaveFormat.wBitsPerSample = BitsPerSample;
    NewWaveFormat.cbSize = CBSize;
    
    REFERENCE_TIME RequestedAudioDuration = REFTIMES_HZ * 2;
    
    Result = WasapiAudio.AudioClient->lpVtbl->Initialize(WasapiAudio.AudioClient,
                                                         AUDCLNT_SHAREMODE_SHARED,
                                                         AUDCLNT_STREAMFLAGS_RATEADJUST | AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM | AUDCLNT_STREAMFLAGS_SRC_DEFAULT_QUALITY,
                                                         RequestedAudioDuration,
                                                         0,
                                                         &NewWaveFormat,
                                                         0);
    
    if(Result != S_OK)
    {
        printf("Error initializing WASAPI audio client! Error: %u\n", Result);
        return false;
    }
    
    WasapiAudio.LatencyFrameCount = WasapiAudio.SamplesHz / SOUND_LATENCY_FPS;
    
    Result = WasapiAudio.AudioClient->lpVtbl->GetService(WasapiAudio.AudioClient,
                                                         &IID_IAudioRenderClient,
                                                         (void**) &WasapiAudio.AudioRenderClient);
    
    if(Result != S_OK)
    {
        printf("Error getting audio render client service!");
        return false;
    }
    
    WasapiAudio.AudioClient->lpVtbl->GetBufferSize(WasapiAudio.AudioClient,
                                                   &WasapiAudio.BufferFrameCount);
    WasapiAudio.SoundBufferDuration = (REFERENCE_TIME) ((r64) REFTIMES_HZ * (WasapiAudio.BufferFrameCount / WasapiAudio.SamplesHz));
    WasapiAudio.AudioClient->lpVtbl->Start(WasapiAudio.AudioClient);
    return true;
}

u32 FramesToWrite()
{
    u32 FramePadding = 0;
    HRESULT Result = WasapiAudio.AudioClient->lpVtbl->GetCurrentPadding(WasapiAudio.AudioClient, &FramePadding);
    u32 FramesToWrite = WasapiAudio.LatencyFrameCount > FramePadding ? WasapiAudio.LatencyFrameCount - FramePadding : 0;
    return FramesToWrite;
}

void WriteSoundBuffer(r32* Samples, u32 NumFrames)
{
    u8* Data = NULL;
    DWORD Flags = 0;
    
    WasapiAudio.AudioRenderClient->lpVtbl->GetBuffer(WasapiAudio.AudioRenderClient, NumFrames, &Data);
    if(Data)
    {
        u32 StartSample = (WasapiAudio.FramesPlayed % 48000) * 2;
        i16* Dest = (i16*) Data;
        u32 NumSamples = NumFrames * WasapiAudio.NumChannels;
        for(u32 Idx = 0; Idx < NumSamples; ++Idx)
        {
            i16 Sample = (i16) (Samples[(StartSample + Idx) % 96000] * 32768.0f);
            *Dest++ = Sample;
        }
    }
    
    WasapiAudio.AudioRenderClient->lpVtbl->ReleaseBuffer(WasapiAudio.AudioRenderClient, NumFrames, Flags);
    
    WasapiAudio.FramesPlayed += NumFrames;
}


platform_api Win32PlatformAPI = { .InitializeAudio = LoadAndInitializeWASAPI, .FramesToWrite = FramesToWrite, .WriteSoundBuffer = WriteSoundBuffer };

platform_api GetWin32WASAPIBackend()
{
    return Win32PlatformAPI;
}