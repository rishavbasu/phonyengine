
#include "audio.h"
#include "wav_loader.h"
#include "phony_math.h"

b32 load_sound_from_filename(audio_system* audio, const char* file_name) {
    if(read_wav_file(file_name, &audio->loaded_sounds[audio->num_loaded_sounds])) {
        audio->num_loaded_sounds++;
        return true;
    }
    return false;
}

voice_instance* voice_from_id(audio_system* audio, u32 id) {
    u32 index = id & VOICE_INDEX_MASK;
    u32 generation = id >> VOICE_INDEX_BITS;
    voice_instance* instance = &audio->voices[index];
    if(instance->generation != generation)
        return NULL;
    return instance;
}

voice_instance* find_free_voice(audio_system* audio) {
    voice_instance* free_instance = NULL;
    if(audio->free_voice_list) {
        free_instance = audio->free_voice_list;
        audio->free_voice_list = audio->free_voice_list->next;
        if(audio->free_voice_list)
            audio->free_voice_list->prev = NULL;
    }
    else {
        for(u32 i = 0; i < MAX_VOICES_PLAYING; ++i) {
            if(!has_voice_flags(&audio->voices[i], PY_VOICE_FLAGS_PLAYING)) {
                free_instance = &audio->voices[i];
                break;
            }
        }
    }
    return free_instance;
}

void remove_voice(audio_system* audio, voice_instance* voice) {
    remove_voice_flags(voice, PY_VOICE_FLAGS_PLAYING);
    voice_instance* old_free_list_head = audio->free_voice_list;
    if(old_free_list_head)
        old_free_list_head->prev = voice;
    voice->next = old_free_list_head;
    voice->prev = NULL;
    audio->free_voice_list = voice;
}

void add_voice_flags(voice_instance* voice, voice_flags flags) { 
    voice->flags |= flags;
}
void remove_voice_flags(voice_instance* voice, voice_flags flags) {
    voice->flags &= ~flags;
}

b32 has_voice_flags(voice_instance* voice, voice_flags flags) {
    return (voice->flags & flags) == flags;
}

u32 play_sound(audio_system* audio, u32 sound_id) {
    voice_instance* voice = find_free_voice(audio);
    if(voice == NULL)
        return U32_MAX;
    
    add_voice_flags(voice, PY_VOICE_FLAGS_PLAYING);
    voice->generation = voice->generation + 1;
    voice->volume = 1.0f;
    voice->current_sample = 0;
    voice->sound_id = sound_id;
    
    u32 index = (u32) (voice - &audio->voices[0]);
    return (voice->generation << VOICE_INDEX_BITS) + index;
}

u32 play_sound_at_pos_2d(audio_system* audio, u32 sound_id, vec2 pos) {
    voice_instance* voice = find_free_voice(audio);
    if(voice == NULL)
        return U32_MAX;
    
    add_voice_flags(voice, PY_VOICE_FLAGS_PLAYING|PY_VOICE_FLAGS_2D);
    voice->generation = voice->generation + 1;
    voice->volume = 1.0f;
    voice->current_sample = 0;
    voice->sound_id = sound_id;
    voice->pos_2d = pos;
    
    u32 index = (u32) (voice - &audio->voices[0]);
    return (voice->generation << VOICE_INDEX_BITS) + index;
}

u32 add_sythesizer_voice(audio_system* audio, synthesize_func synthesizer, void* payload)
{
    voice_instance* voice = find_free_voice(audio);
    if(voice == NULL)
        return U32_MAX;
    
    add_voice_flags(voice, PY_VOICE_FLAGS_PLAYING|PY_VOICE_FLAGS_SYNTHESIZED_SOUND);
    voice->generation = voice->generation + 1;
    voice->synthesizer = synthesizer;
    voice->payload    = payload;
    u32 index = (u32) (voice - &audio->voices[0]);
    return (voice->generation << VOICE_INDEX_BITS) + index;
}

void fade_voice_out(audio_system* audio, u32 voice_id, r32 time_to_fade_out) {
    voice_instance* voice = voice_from_id(audio, voice_id);
    if(voice) {
        add_voice_flags(voice, PY_VOICE_FLAGS_FADING|PY_VOICE_FLAGS_REMOVE_AFTER_FADE);
        voice->target_volume = 0.0f;
        voice->fade_start_sample = voice->current_sample;
        voice->fade_end_sample = voice->fade_start_sample + (u32) (48000 * time_to_fade_out);
    }
}

void render_audio(audio_system* audio, u32 num_frames_to_write) {
    u32 num_samples = num_frames_to_write * 2; // assumes 2 channels
    u32 start_sample = (audio->master_bus.frames_played % 48000) * 2;
    for(u32 sample = 0; sample < num_samples; ++sample) {
        audio->master_bus.buffer[(start_sample + sample) % 96000] = 0.0f;
    }
    
    for(u32 voice_index = 0; voice_index < MAX_VOICES_PLAYING; ++voice_index) {
        voice_instance* voice = &audio->voices[voice_index];
        if(has_voice_flags(voice, PY_VOICE_FLAGS_PLAYING) ) {
            if(has_voice_flags(voice, PY_VOICE_FLAGS_SYNTHESIZED_SOUND)) {
                voice->synthesizer(&audio->master_bus, num_frames_to_write, 2, voice->payload);
            }
            else {
                sound_data* data = &audio->loaded_sounds[voice->sound_id];
                i32 samples_to_mix = PY_MIN(num_samples, data->num_samples - voice->current_sample);
                r32 volume = voice->volume * 2.0f;
                if(has_voice_flags(voice, PY_VOICE_FLAGS_2D))
                {
                    r32 r_pan = (py_vec2_dot(py_vec2_make(1.0f, 0.0f), py_vec2_normalized_safe(voice->pos_2d)) + 1.0f) / 2.0f;
                    r32 l_pan = 1.0f - r_pan;
                    r32 dist = py_vec2_dist(py_vec2_make(0.0f, 0.0f), voice->pos_2d);
                    //r32 attenuation = 1.0f / py_log2_r32(dist + 2.0f);
                    r32 log_factor = 1.0f / (dist + 1.0f);
                    r32 log_attenuation = (log_factor * 1.0f / py_log2_r32(dist + 2.0f));
                    r32 inverse_attenuation = (1.0f - log_factor) * (1.0f / py_r32_square(dist + 1.0f));
                    r32 attenuation = log_attenuation + inverse_attenuation;
                    if(has_voice_flags(voice, PY_VOICE_FLAGS_FADING)) {
                        i32 num_samples_to_fade = voice->fade_end_sample - voice->fade_start_sample;
                        for(i32 sample = 0; sample < samples_to_mix; ++sample) {
                            i32 current_sample = sample + voice->current_sample;
                            r32 faded_volume = py_r32_lerp(voice->volume,
                                                           voice->target_volume,
                                                           ((r32) current_sample - voice->fade_start_sample) / ((r32)num_samples_to_fade));
                            i16 sample_value;
                            memcpy(&sample_value, &data->data[(sample + voice->current_sample) * 2], data->num_channels);
                            audio->master_bus.buffer[(start_sample + sample) % 96000] += faded_volume * ((r32) sample_value / 32768.0f);
                        }
                    }
                    else {
                        for(i32 frame = 0; frame < (samples_to_mix / (i32) data->num_channels); ++frame) {
                            i16 left_sample_value;
                            i16 right_sample_value;
                            memcpy(&left_sample_value, &data->data[(frame * data->num_channels + voice->current_sample) * data->bytes_per_sample], data->bytes_per_sample);
                            memcpy(&right_sample_value, &data->data[(frame * data->num_channels + 1 + voice->current_sample) * data->bytes_per_sample], data->bytes_per_sample);
                            audio->master_bus.buffer[(start_sample + frame * data->num_channels) % 96000] += l_pan * attenuation * volume * ((r32) left_sample_value / 32768.0f);
                            audio->master_bus.buffer[(start_sample + frame * data->num_channels + 1) % 96000] += r_pan * attenuation * volume * ((r32) right_sample_value / 32768.0f);
                        }
                    }
                }
                else
                {
                    if(has_voice_flags(voice, PY_VOICE_FLAGS_FADING)) {
                        i32 num_samples_to_fade = voice->fade_end_sample - voice->fade_start_sample;
                        for(i32 sample = 0; sample < samples_to_mix; ++sample) {
                            i32 current_sample = sample + voice->current_sample;
                            r32 faded_volume = py_r32_lerp(voice->volume,
                                                           voice->target_volume,
                                                           ((r32) current_sample - voice->fade_start_sample) / ((r32)num_samples_to_fade));
                            i16 sample_value;
                            memcpy(&sample_value, &data->data[(sample + voice->current_sample) * 2], data->num_channels);
                            audio->master_bus.buffer[(start_sample + sample) % 96000] += faded_volume * ((r32) sample_value / 32768.0f);
                        }
                    }
                    else {
                        for(i32 sample = 0; sample < samples_to_mix; ++sample) {
                            i16 sample_value;
                            memcpy(&sample_value, &data->data[(sample + voice->current_sample) * 2], data->num_channels);
                            audio->master_bus.buffer[(start_sample + sample) % 96000] += volume * ((r32) sample_value / 32768.0f);
                        }
                    }
                }
                voice->current_sample += samples_to_mix;
                b32 voice_ended = voice->current_sample == (i32) data->num_samples;
                b32 fade_ended = has_voice_flags(voice, PY_VOICE_FLAGS_FADING|PY_VOICE_FLAGS_REMOVE_AFTER_FADE) &&
                    (voice->current_sample >= voice->fade_end_sample);
                if(voice_ended || fade_ended)
                    remove_voice(audio, voice);
            }
        }
    }
}

void reload_audio_system(audio_system* audio) {
}
void free_audio_resources(audio_system* audio) {
}