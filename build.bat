@ECHO off
echo Compile %1, Configuration: %2
set CompileTest="false"
set CompileLibrary="false"
set Configuration="%2"
if %1 == both (
	set CompileTest="true"
	set CompileLibrary="true"
)
if %1 == "test" (
	set CompileTest="true"
)
if %1 == "lib" (
	set CompileLibrary="true"
)
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
set path=w:\C Backend\misc;%path%;

set CommonCompilerFlags=/DEBUG:FAST /O2 /MP /MTd /nologo /fp:fast /Gm- /GR- /EHa /Zo /Oi /WX /W4 /wd4201 /wd4324 /wd4100 /wd4189 /wd4505 /wd4127 /wd4152 /Z7 /FC /F67108864 /std:c++17
set CommonLinkerFlags= /INCREMENTAL

if %Configuration%=="debug" (
	set CommonCompilerFlags=/DEBUG:FULL /Od /MTd /MP /nologo /fp:fast /Gm- /GR- /EHa /Zo /Oi /WX /W4 /wd4201 /wd4324 /wd4100 /wd4189 /wd4505 /wd4127 /wd4152 /Z7 /FC /F67108864 /D_PHONY_DEBUG /std:c++17
	set CommonLinkerFlags= /INCREMENTAL:NO /OPT:NOREF
)

mkdir builds
pushd builds

set GLFWSrc= ..\code\vendors\glfw\src\context.c ..\code\vendors\glfw\src\init.c ..\code\vendors\glfw\src\input.c ..\code\vendors\glfw\src\monitor.c ..\code\vendors\glfw\src\vulkan.c ..\code\vendors\glfw\src\window.c ..\code\vendors\glfw\src\win32_init.c ..\code\vendors\glfw\src\win32_joystick.c ..\code\vendors\glfw\src\win32_monitor.c ..\code\vendors\glfw\src\win32_time.c ..\code\vendors\glfw\src\win32_thread.c ..\code\vendors\glfw\src\win32_window.c ..\code\vendors\glfw\src\wgl_context.c ..\code\vendors\glfw\src\egl_context.c ..\code\vendors\glfw\src\osmesa_context.c

set IMGUISrc= ..\code\vendors\imgui\imgui.cpp ..\code\vendors\imgui\backends\imgui_impl_opengl3.cpp ..\code\vendors\imgui\backends\imgui_impl_glfw.cpp ..\code\vendors\imgui\imgui_draw.cpp ..\code\vendors\imgui\imgui_widgets.cpp ..\code\vendors\imgui\imgui_tables.cpp ..\code\vendors\imgui\implot.cpp ..\code\vendors\imgui\implot_items.cpp

set PlatformSrc= ..\code\main.cpp ..\code\vendors\glad\src\glad.c ..\code\win32_wasapi_backend.c ..\code\audio.c ..\code\synthesizer.c ..\code\wav_loader.c

if %CompileTest%=="true" (
	cl /D_CRT_SECURE_NO_WARNINGS /D_GLFW_WIN32 /DBACKEND_DEBUG %CommonCompilerFlags% %PlatformSrc% ..\code\glfw_backend.c /link %CommonLinkerFlags% shell32.lib user32.lib gdi32.lib winmm.lib
)
 
PAUSE